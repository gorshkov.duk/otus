import org.testng.Assert;
import org.testng.annotations.Test;

public class VectorTest {

    @Test
    public void testSumVector() {
        Vector v1 = new Vector(new int[] {3, 5});
        Vector v2 = new Vector(new int[] {3, -5});
        Vector resultVector = Vector.sumVector(v1, v2);

        Assert.assertEquals(new int[] {6, 0}, resultVector.getBody());
    }
}
import static org.mockito.Mockito.*;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

@Test
public class MovableAdapterTest {
    protected GameObject tank;
    protected MovableAdapter adapter;

    @BeforeTest
    protected void setUp() {
        tank = mock(GameObject.class);
        adapter = new MovableAdapter(tank);
        when(tank.getValue("position")).thenThrow(IllegalArgumentException.class);
        when(tank.getValue("velocity")).thenThrow(IllegalArgumentException.class);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class})
    public void readPositionExceptionTest() {
        adapter.getPosition();
    }

    @Test(expectedExceptions = {IllegalArgumentException.class})
    public void readVelocityExceptionTest() {
        adapter.getPosition();
    }
}

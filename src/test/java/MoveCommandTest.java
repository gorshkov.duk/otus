import static org.mockito.Mockito.*;

import org.testng.annotations.Test;

@Test
public class MoveCommandTest {

    @Test
    public void testExecute() {
        GameObject tank = mock(GameObject.class);
        Vector positionVector = new Vector(new int[] {12, 5});
        Vector velocityVector = new Vector(new int[] {-7, 3});
        Vector resultPosition = new Vector(new int[] {5, 8});

        when(tank.getValue("position")).thenReturn(positionVector);
        when(tank.getValue("velocity")).thenReturn(velocityVector);

        MoveCommand command = new MoveCommand(new MovableAdapter(tank));
        command.execute();

        verify(tank).setValue(eq("position"), refEq(resultPosition));
    }
}

import static org.mockito.Mockito.*;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

@Test
public class RotableAdapterTest {
    protected GameObject tank;
    protected RotableAdapter adapter;

    @BeforeTest
    public void setUp() {
        tank = mock(GameObject.class);
        adapter = new RotableAdapter(tank);

        when(tank.getValue("direction")).thenThrow(IllegalArgumentException.class);
        when(tank.getValue("velocity")).thenThrow(IllegalArgumentException.class);
        when(tank.getValue("maxDirection")).thenThrow(IllegalArgumentException.class);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class})
    public void readDirectionExceptionTest() {
        adapter.getDirection();
    }

    @Test(expectedExceptions = {IllegalArgumentException.class})
    public void readVelocityExceptionTest() {
        adapter.getAngularVelocity();
    }

    @Test(expectedExceptions = {IllegalArgumentException.class})
    public void readMaxDirectionExceptionTest() {
        adapter.getMaxDirections();
    }
}

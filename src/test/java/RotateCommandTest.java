import static org.mockito.Mockito.*;
import org.testng.annotations.Test;

public class RotateCommandTest {

    @Test
    public void testExecute() {
        GameObject tankMock = mock(GameObject.class);
        int direction = 4;
        int velocity = 5;
        int maxDirection = 8;
        int resultDirection = 1;

        when(tankMock.getValue("direction")).thenReturn(direction);
        when(tankMock.getValue("velocity")).thenReturn(velocity);
        when(tankMock.getValue("maxDirection")).thenReturn(maxDirection);

        RotateCommand command = new RotateCommand(new RotableAdapter(tankMock));
        command.execute();

        verify(tankMock).setValue("direction", resultDirection);
    }
}
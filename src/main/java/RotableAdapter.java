public class RotableAdapter implements Rotable {
    protected GameObject object;

    public RotableAdapter(GameObject object) {
        this.object = object;
    }

    @Override
    public int getDirection() {
        return (int) object.getValue("direction");
    }

    @Override
    public void setDirection(int direction) {
        object.setValue("direction", direction);
    }

    @Override
    public int getAngularVelocity() {
        return (int) object.getValue("velocity");
    }

    @Override
    public int getMaxDirections() {
        return (int) object.getValue("maxDirection");
    }
}

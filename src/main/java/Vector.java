public class Vector {
    protected int[] body;

    /**
     * Vector constructor
     * @param body int[]
     */
    public Vector(int[] body) {
        this.body = body;
    }

    /**
     * Body getter
     * @return int[]
     */
    public int[] getBody() {
        return body;
    }

    /**
     * Сложение векторов
     * @param v1 Vector
     * @param v2 Vector
     * @return Vector
     */
    public static Vector sumVector(Vector v1, Vector v2) {
        int[] tmpBody = new int[v1.body.length];

        for(int i = 0; i < tmpBody.length; i++) {
            tmpBody[i] = v1.body[i] + v2.body[i];
        }

        return new Vector(tmpBody);
    }
}

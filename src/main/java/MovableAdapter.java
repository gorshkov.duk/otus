public class MovableAdapter implements Movable {
    protected GameObject object;

    public MovableAdapter(GameObject object) {
        this.object = object;
    }

    @Override
    public Vector getPosition() {
        return (Vector) object.getValue("position");
    }

    @Override
    public void setPosition(Vector vct) {
        object.setValue("position", vct);
    }

    @Override
    public Vector getVelocity() {
        return (Vector) object.getValue("velocity");
    }
}

public interface Movable {
    Vector getPosition();
    void setPosition(Vector vct);
    Vector getVelocity();
}

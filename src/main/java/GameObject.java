public interface GameObject {
    Object getValue(String value);
    void setValue(String value, Object newValue);
}

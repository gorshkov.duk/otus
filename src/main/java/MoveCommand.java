public class MoveCommand implements Command {
    private final Movable movable;

    public MoveCommand(Movable movable) {
        this.movable = movable;
    }

    public void execute()
    {
        movable.setPosition(
                Vector.sumVector(
                        movable.getPosition(),
                        movable.getVelocity()
                )
        );
    }
}
